users = {"chris": "helloworld", "john": "passw1", "nelly": "2hell1", "wendy": "1Passw"}

def check(password):
    if not password[0].isdigit():
        return False
    if not len(password) >= 6:
        return False
    count = 0
    for character in password:
        if character.isupper():
            count += 1
    if count == 1:
        return True
    else:
        return False

for navn, passord in users.items():
    if check(passord):
        print(passord, navn)