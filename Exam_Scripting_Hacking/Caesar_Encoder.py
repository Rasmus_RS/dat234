def caesar_encoder(input, lineshift):

    #variabel definisjon
    characters = 'abcdefghijklmnopqrstuvwxyz'
    result = ""
    input = input.lower()


    #for loop, sjekker om det er alfabetisk karakter og legger til i liste.
    for l in input:  ##param l = letter, lst = list of index.
        if l in characters:
            lst = characters.find(l)
            lst = (lst + lineshift) % (len(characters))
            result = result + characters[lst]
        else:
            result = result + l
    return result + " "

#åpner ordliste, lager ny liste fra ord som har splittet og legger til ord inn i liste.
with open("test2.txt") as DictFile:
    list_of_words = []
    for word in DictFile:
        list_from_file = word.split(",")
        for split_word in list_from_file:
            list_of_words.append(split_word)
    encrypted_list = []

#krypterer ordene i lista. ordene blir kryptert med linjeskift. linjeskift kan være positiv eller negativ.
for word in list_of_words:
    encrypted_list.append(caesar_encoder(word, 3))


#lager en ny fil, der den skriver inn de krypterte ordene.
with open("Encodedtest.txt", "w+") as file:
    for w in encrypted_list:
        file.write(w)

    file.close()