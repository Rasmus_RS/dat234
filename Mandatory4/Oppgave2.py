import os
from collections import OrderedDict
try:
    myfile = open("passwordlist.txt", "r")

    file = myfile.read().split(";")

    filtered_list = {}
    counter = 0

    with open('passwordlist.txt', 'rb+') as f:
        f.seek(-1, os.SEEK_END)
        if os.SEEK_END == str(";"):
            f.truncate()

    for element in file:
        split_element = element.split(":")
        bruker = split_element[0]
        passord = split_element[1]

        if bruker not in filtered_list:
            filtered_list[bruker] = [passord]
        else:
            filtered_list[bruker].append(passord)
        counter += 1

    #loop som printer brukernavn med tilh&oslash;rende passord
    for element in filtered_list:
        buriedArray = filtered_list.get(element)
        print("The passwords for user " + element + " are: ")
        print(buriedArray)
    #Teller totalt antall passord
    print(counter, "Passord totalt")

    #Printer alle unike brukernavn
    for bruker in filtered_list:
        print(bruker)
        print("-----------")
    #Printer alle unike passord
    for bruker in filtered_list:
        if bruker in filtered_list:
            passordArray = filtered_list[bruker]
        print(passordArray)

#oppgave4
except IOError:print("file does not exist")
#if file does not exist: exit gracefully. see oppgave3 for more detail. 
