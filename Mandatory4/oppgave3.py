try:
    with open("passwordlist.txt", "r") as readfile:

        file = readfile.read().split(";")
        filtered_list = {}

    for element in file:
        split_element = element.split(":")
        bruker = split_element[0]
        passord = split_element[1]

        if bruker not in filtered_list:
            filtered_list[bruker] = [passord]
        else:
            filtered_list[bruker].append(passord)

        with open("newlist.txt", "w+") as wf:
            str1 = str(filtered_list)
            wf.write("Username : Passwords\n")
            wf.write("--------------------------------------------------------------------------------\n")
            wf.write(str1.replace("],", "\n").replace("'", " ").replace("{", " ").replace("[", " "))

            # This output file is obviously more readable than the previous version
            # We made the output so that the usernames are on one side,
            # and their passwords are next to their username in their own bracket.
            # We feel that this is the best solution, because it gives the best possible overview
            # without showing the username multiple times.

#oppgave4
except IOError:
    print("file does not exist")
# EAFP error handling when file does not exist makes sense,
# since you exit gracefully when the input file is non existant.