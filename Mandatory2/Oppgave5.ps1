﻿$userInput = Read-Host "Please enter some text"
$userInput |  ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString |  Set-Content -Path secret.txt

Get-Content secret.txt

$secretStuff = Get-Content  -Path secret.txt | ConvertTo-SecureString
[Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($secretStuff)))) 


# In this task we used securestring to encrypt the input from a user, and store it in a text-file.
# This is a slight improvement over the other algorithm we used, since it can be stored more securely. 
# SecureString is a feature from Powershell and is a decent option for encrypting strings. 
# The problem here is that the string is stored in a txt-file. It should therefore be kept in a secure database, or given
# strict NTFS rights. It should not be accessible, even though the content is encrypted. 